// const imageObserverOptions = {
//   root: document.querySelector('body'),
//   rootMargin: '100px 0px 0px',
// }

// const imageObserverCb = (entries, observer) => {
//    entries.forEach((entry) => {

//     if (entry.isIntersecting) {

//       const imagePath = entry.target.dataset.image
//       switch (entry.target.tagName) {
//         case 'IMG': {
//           entry.target.src = imagePath
//         }
//         default: {
//           entry.target.style.backgroundImage = `url('${imagePath}')`
//         }
//       }

//       observer.unobserve(entry.target)
//     }
//   })
// }

// const imageObserver = new IntersectionObserver(
//   imageObserverCb,
//   imageObserverOptions
// )

// const imageObserverMain = () => {
//   let imageKeepers = document.querySelectorAll('[data-image]')
//   if (imageKeepers.length > 0) {
//     imageKeepers = [...imageKeepers].map((img) => {
//       imageObserver.observe(img)
//     })
//   }
// }

class ImageObserver {
  params = {
    rootMargin: '100px 0px 0px',
  }

  selector = '[data-image]'

  imageObserverCb = (entries, observer) => {
    entries.forEach((entry) => {
      if (entry.isIntersecting) {
        const imagePath = entry.target.dataset.image
        switch (entry.target.tagName) {
          case 'IMG': {
            entry.target.src = imagePath
          }
          default: {
            entry.target.style.backgroundImage = `url('${imagePath}')`
          }
        }
        delete entry.target.dataset.image
        observer.unobserve(entry.target)
      }
    })
  }

  imageObserver = new IntersectionObserver(
    this.imageObserverCb,
    this.params
  )

  imageObserverStart = () => {
    let imageKeepers = document.querySelectorAll('[data-image]')
    if (imageKeepers.length > 0) {
      [...imageKeepers].forEach((img) => {
        this.imageObserver.observe(img)
      })
    }
  }
}
