class HtmlRender {
  constructor(selector)
  {
    this.parent = document.querySelector(selector)
  }
  rootDir = './dist'
  data = []
  render = () => {
    if (this.data.length > 0) {
      const html = this.data.map((elem) => this.template(elem))
      if (this.parent) {
        this.parent.innerHTML = html.join('')
      }
    }
  }
  template = (data) => {
    return data
  } 
}