class FeaturedPosts extends HtmlRender {
  constructor(selector) {
    super(selector)
  }

  data = [
    {
      img: '/img/featured_posts/featured_post1.png',
      title: 'the road ahead',
      description: 'The road ahead might be paved - it might not be.',
      user: {
        avatar: '/img/user/user2.png',
        name: 'mat vogels',
      },
      date: 'September 25, 2015',
    },
    {
      img: '/img/featured_posts/featured_post2.png',
      title: 'from top down',
      description: 'Once a year, go someplace you’ve never been before.',
      user: {
        avatar: '/img/user/user1.png',
        name: 'william wong',
      },
      date: 'September 25, 2015',
    },
  ]


// style="background-image: url(./dist${img}"
// src="./dist${user.avatar}"

  template = ({ img, title, description, date, user }) => {
    return `
    <li
      class="post-featured-item"
      data-image="${this.rootDir + img}"
     
      >
      <div class="card">
        <h5 class="card__title post-featured-item__title">${title}</h5>
        <p class="card__description post-featured-item__description">
          ${description}
        </p>
        <div class="card__footer">
          <a class="card__footer-user post-featured-item__footer-user" href="#">
            <div class="card__footer-image">
              <img 
                alt="${user.name}" 
                data-image="${this.rootDir + user.avatar}"
              />
            </div>
            <p class="card__footer-username">${user.name}</p>
          </a>
          <p class="card__footer-date post-featured-item__footer-date">${date}</p>
        </div>
      </div>
    </li>
    `
  }
}