class RecentPosts extends HtmlRender {
  constructor(selector) {
    super(selector)
  }
  data = [
    {
      img: '/img/recent-posts/recent_post1.png',
      title: 'still standing tall',
      description: 'Life begins at the end of your comfort zone.',
      user: {
        avatar: '/img/user/user2.png',
        name: 'william wong',
      },
      date: '9/25/2015',
    },
    {
      img: '/img/recent-posts/recent_post2.png',
      title: 'sunny side up',
      description: 'No place is ever as bad as they tell you it’s going to be.',
      user: {
        avatar: '/img/user/user1.png',
        name: 'mat vogels',
      },
      date: '9/25/2015',
    },
    {
      img: '/img/recent-posts/recent_post3.png',
      title: 'water falls',
      description: 'We travel not to escape life, but for life not to escape us.',
      user: {
        avatar: '/img/user/user1.png',
        name: 'mat vogels',
      },
      date: '9/25/2015',
    },
    {
      img: '/img/recent-posts/recent_post4.png',
      title: 'through the mist',
      description: 'Travel makes you see what a tiny place you occupy in the world.',
      user: {
        avatar: '/img/user/user1.png',
        name: 'mat vogels',
      },
      date: '9/25/2015',
    },
    {
      img: '/img/recent-posts/recent_post5.png',
      title: 'Awaken Early',
      description: 'Not all those who wander are lost.',
      user: {
        avatar: '/img/user/user1.png',
        name: 'mat vogels',
      },
      date: '9/25/2015',
    },
    {
      img: '/img/recent-posts/recent_post6.png',
      title: 'Try it Always',
      description: 'The world is a book, and those who do not travel read only one page.',
      user: {
        avatar: '/img/user/user2.png',
        name: 'william wong',
      },
      date: '9/25/2015',
    },
  ]
  // src="./dist${img}"
  // src="${user.avatar}"
  template = ({ img, title, description, date, user }) => {
    return `
      <li class="post-recent-item">
        <div class="post-recent-item__img">
          <img
            data-image="${this.rootDir + img}"
            alt="${title}"
          />
        </div>
        <div class="card">
          <h5 class="card__title post-recent-item__title">${title}</h5>
          <p class="card__description post-recent-item__description">${description}</p>
          <div class="card__footer post-recent-item__footer">
            <a class="card__footer-user post-recent-item__footer-user" href="#">
              <div class="card__footer-image">
                <img
                  data-image="${this.rootDir + user.avatar}"
                  alt="${user.name}"
                />
              </div>
              <p class="card__footer-username">${user.name}</p>
            </a>
            <p class="card__footer-date post-recent-item__footer-date">${date}</p>
          </div>
        </div>
      </li>
    `
  }
}
